<?php

namespace Drupal\sbr_places;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\sbr_places\Entity\PlaceInterface;

/**
 * Defines the storage handler class for Place entities.
 *
 * This extends the base storage class, adding required special handling for
 * Place entities.
 *
 * @ingroup sbr_places
 */
interface PlaceStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Place revision IDs for a specific Place.
   *
   * @param \Drupal\sbr_places\Entity\PlaceInterface $entity
   *   The Place entity.
   *
   * @return int[]
   *   Place revision IDs (in ascending order).
   */
  public function revisionIds(PlaceInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Place author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Place revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\sbr_places\Entity\PlaceInterface $entity
   *   The Place entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(PlaceInterface $entity);

  /**
   * Unsets the language for all Place with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
