<?php

namespace Drupal\sbr_places\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Place entities.
 *
 * @ingroup sbr_places
 */
class PlaceDeleteForm extends ContentEntityDeleteForm {


}
